    
    <script src="https://use.typekit.net/ryf2lug.js"></script>
    <script src="scripts/drawCard.js"></script>
    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>


    <!-- Database Script -->
    <script src="scripts/script.js"></script>


    <!-- Google Maps -->
    <script src="scripts/maps.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGSn6rECGTYW9VMQoSSq3oZ0l_focaD0I&callback=myMap"></script>

    <script src="scripts/googleanalytics.js"></script>