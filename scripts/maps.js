function myMap() {
    var myCenter = new google.maps.LatLng(45.4060, -73.9419);
    var mapCanvas = document.getElementById("map");
    var mapOptions = {
        center: myCenter,
        zoom: 15
    };
    var map = new google.maps.Map(mapCanvas, mapOptions);
    var marker = new google.maps.Marker({
        position: myCenter
    });
    marker.setMap(map);

    var infowindow = new google.maps.InfoWindow({
        content: "EasyGo Hotels Headquarters"
    });
    
    map.setOptions({draggable: false, zoomControl: false, scrollwheel: false, disableDoubleClickZoom: true});
    
    infowindow.open(map, marker);
}

function myMap2() {
    var myCenter = new google.maps.LatLng(43.761539, -79.411079);
    var mapCanvas = document.getElementById("map2");
    var mapOptions = {
        center: myCenter,
        zoom: 15
    };
    var map = new google.maps.Map(mapCanvas, mapOptions);
    var marker = new google.maps.Marker({
        position: myCenter
    });
    marker.setMap(map);

    var infowindow = new google.maps.InfoWindow({
        content: "Toronto Inn"
    });
    
    map.setOptions({draggable: false, zoomControl: false, scrollwheel: false, disableDoubleClickZoom: true});
    
    infowindow.open(map, marker);
}