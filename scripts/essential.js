     <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Rooms | EasyGo - Your best way to book hotel rooms in Canada</title>
    <meta name="description" content="EasyGo - Your best way to book hotel rooms in Canada">
    <!-- Insert favicon.ico here later -->

    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="https://opensource.keycdn.com/fontawesome/4.7.0/font-awesome.min.css" integrity="sha384-dNpIIXE8U05kAbPhy3G1cz+yZmTzA6CY8Vg/u2L9xRnHjJiAK76m2BIEaSEV+/aU" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="styles/styles.css">

    <!-- Typekit Font -->
    <script src="https://use.typekit.net/ryf2lug.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Database Script -->
    <script src="scripts/script.js"></script>


    <!-- Google Maps -->
    <script src="scripts/maps.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGSn6rECGTYW9VMQoSSq3oZ0l_focaD0I&callback=myMap"></script>

    <script src="scripts/googleanalytics.js"></script>
   
     <script type="text/javascript" src="scripts/script.js"></script>
        <script type="text/javascript" src="scripts/init.js"></script>
      <script type="text/javascript" src="scripts/finalDraw.js"></script>
    
